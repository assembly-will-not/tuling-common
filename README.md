# Tuling Common

## 简介

**Tuling Common** 是一个基于 Spring Boot、Sa-Token 和 MyBatis-Plus 构建的快速开发框架。该框架无需编写额外代码即可实现基础的增删改查功能，集成了多个实用模块，帮助开发者快速构建应用程序。

### 核心功能模块

- **权限管理**: 基于 Sa-Token 实现安全认证和授权，提供可靠的访问控制。
- **系统管理**: 提供用户、角色、菜单等基础管理功能。
- **微信公众号**: 集成微信公众号相关功能，如消息推送、自定义菜单等，快速对接微信应用程序。
- **EasyExcel**: 扩展了 EasyExcel，能高效处理大数据量的导入。

## 项目结构

tuling-common  
├── tuling-common-bom                                       # BOM文件  
├── tuling-common-core                                      # 核心通用类  
├── tuling-common-excel                                     # EasyExcel扩展，高效导入大量数据  
├── tuling-common-mybatis                                   # MyBatis-Plus封装，支持租户隔离  
├── tuling-common-printer                                   # 打印组件封装  
├── tuling-common-redis                                     # Redis封装  
├── tuling-common-satoken                                   # Sa-Token封装，简化上下文使用  
├── tuling-common-utils                                     # 通用工具类  
├── tuling-common-web                                       # 封装CRUD通用类  
└── tuling-modules                                          # 通用业务模块  
├── tuling-auth                                             # 鉴权模块  
├── tuling-log                                              # 日志模块  
├── tuling-security                                         # 加密模块  
├── tuling-system                                           # 系统模块  
└── tuling-wxmq                                             # 微信公众号集成  